#include <stdio.h>
#include "sudoku.h"
#include "sudoku_io.h"


int main(int argc, char** argv) {
    unsigned short grid[81];
    if(argc > 1) {
        printf("Input Value <...> is:\n<%s>\n", argv[1]);
        load_sudoku(argv[1], grid);
        printf("Input Sudoku is:\n");
        print_sudoku(grid);
        solve(grid);
        printf("Solved Sudoku is:\n");
        print_sudoku(grid);
    }
}