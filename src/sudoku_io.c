#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "sudoku_io.h"

void load_sudoku(const char* sudoku_str, unsigned short* grid) {
    char c[1];
    short d;
    for(int k=0; k<81; ++k) {
        strncpy(c, sudoku_str+k, 1);
        d = (short) atoi(c);
        // c[0] = sudoku_str[k];
        // printf("Index is: %d, Char is: %s, Short is: %d\n", k, c, d);
        *(grid+k) = d;
    }
}

void load_gameid(const char* id, unsigned short* grid) {
    unsigned short k;
    char c;
    for (k = 0; k < 81; ++k) grid[k] = 0;

    if (strncmp(id, "3:", 2) == 0) {
        for (k = 0; k < 2; ++k) id++;
    }
    else if (strncmp(id, "3x3:", 2) == 0) {
        for (k = 0; k < 4; ++k) id++;
    }
    while(*id) {
        if (isdigit((unsigned char) *id)) {
            grid[k] = atoi(id);
        }
        else {
            strncpy(&c, id, 1);
            if (strncmp(id, (const char *) '_', 1) == 0) {
                k += 1;
            }
            else {
                k += (unsigned int) (c - 'a' + 2);
            }
        }
        ++id;
    }
}

void print_sudoku(const unsigned short* grid) {

    for(int i=0;i<9;++i) {
        if (i%3 == 0) printf("|=====|=====|=====|\n");
        for(int j=0;j<9;++j) {
            printf("|%d", grid[i*9+j]);
        }
        printf("|\n");
    }
    printf("|=====|=====|=====|\n");

}

void print_gameid(const unsigned short* grid) {
    unsigned short k;
    unsigned short counter = 0;
    char c;
    printf("3x3:");
    for (k=0; k<81; ++k) {
        if (grid[k] == 0) {
            counter += 1;
        }
        else {
            if (counter == 0) {
                printf("_");
            }
            else if (counter > 0) {
                c = (unsigned char) (((unsigned short) 'a') + counter - 1u);
                printf("%c", c);
            }
            counter = 0;
            printf("%d", grid[k]);
        }
    }
    printf("\n");
}