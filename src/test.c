#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sudoku.h"
#include "sudoku_io.h"

const char* FILENAME_1 = "../test/0.txt\0";
const char* FILENAME_2 = "../test/subig20.txt\0";
const char* FILENAME_3 = "../test/top2365.txt\0";
const unsigned short N_PROGRESS = 60u;
// const unsigned long N = 10001;

enum Eol {LF=0, CR=1, CRLF=2};

unsigned long n_lines(FILE* fp) {
    /// Get the last file position.
    unsigned long n;
    fseek(fp, 0, SEEK_END);
    n = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    return n;
}

enum Eol file_line_separator(FILE* fp) {
    /// Get line separator: line feed, carrier return or both.
    unsigned long n_cr = 0;
    unsigned long n_lf = 0;
    int c;

    fseek(fp, 0, SEEK_SET);
    do {
        c = getc(fp);
        if (c == 10) n_cr += 1;
        if (c == 13) n_lf += 1;

    } while (c != EOF);
    if (n_cr == n_lf) {
        return CRLF;
    }
    if (n_cr > 0) return CR;
    return LF;
}

int test_file(const char * f_name) {
    FILE* fp;
    unsigned int counter;
    unsigned short grid[81];
    unsigned long n;
    char arr[82];
    char progress[N_PROGRESS + 1];
    enum Eol end_of_line;

    // char fmt[100];
    int st;
    int err_fs;
    unsigned short ls_add;
    // snprintf(fmt, 17, "\rCounter: [%%%2ds]", N_PROGRESS);
    for (unsigned short i = 0; i < N_PROGRESS; ++i) progress[i] = ' ';
    progress[N_PROGRESS] = '\0';
    fp = fopen(f_name, "r");

    if (fp == NULL) {
        printf("Missing input file: %s", f_name);
        return 1;
    }

    end_of_line = file_line_separator((FILE*) fp);
    switch (end_of_line) {
        case CRLF:
            ls_add = 2;
            break;
        case CR:
            ls_add = 1;
            break;
        case LF:
            ls_add = 1;
            break;
        default:
            ls_add = 1;
            break;
    }
    n = n_lines((FILE*) fp) / (81 + ls_add);

    for (counter = 0; counter < n; ++counter) {
        for (unsigned short iii = 0; iii < (unsigned short) (N_PROGRESS*(((double) counter+1.)/(double) n)); ++iii) progress[iii] = '=';
        progress[N_PROGRESS*((counter+1)/n)] = '>';
        printf("\rProgress: %3lu%% [%s]", ((counter+1)/n)*100, progress);

        // while (!err_fs) {
        // printf("N=%lu, N_BYTES=%lu, P=%d.\n", n, n_lines((FILE*) fp), counter * 83);
        err_fs = fseek((FILE*) fp, counter * (81+ls_add), SEEK_SET);
        if (err_fs) {
            printf("E: fseek error at counter=%d", counter);
            exit(1);
        }

        fgets(arr, (81 + ls_add - 1), (FILE*) fp);
        // printf("Input Value <...> was:\n<%s>\n", arr);

        load_sudoku(arr, grid);
        // printf("Input Sudoku is:\n");
        // print_sudoku(grid);
        st = solve(grid);
        // print_sudoku(grid);

        if(!st) {
            printf("\nE: Input Value <...> was:\n<%s>\n", arr);
            printf("E: Solved Sudoku is:\n");
            print_sudoku(grid);
            printf("E: ID:\n");
            print_gameid(grid);
            fclose(fp);
            exit(1);
        }
        // counter = counter + 1;
        // printf("\rCounter: [%10lu]", '='*((counter+1)*10)/n);

        // printf("\rCounter: %5d/%5lu", counter + 1, n);

    }

    fclose(fp);
    printf("\n");
    return 0;
}

int main() {
    int st;
    printf("Test file '%s'\n", FILENAME_1);
    st = test_file(FILENAME_1);
    if (st) {
        printf("Test 1 failed!\n");
        return st;
    }

    printf("Test file '%s'\n", FILENAME_2);
    st = test_file(FILENAME_2);
    if (st) {
        printf("Test 2 failed!\n");
        return st;
    }

    printf("Test file '%s'\n", FILENAME_3);
    st = test_file(FILENAME_3);
    if (st) {
        printf("Test 3 failed!\n");
        return st;
    }
}