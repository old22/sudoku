#include <string.h>
#include <stdio.h>
#include "sudoku.h"
#include "sudoku_io.h"
// Cell's guess are 9 numbers: 9 bits. Full 9 bits is 2^9-1=511.
const unsigned short MAX_GUESS = 511;

void idx_mat2cell(const unsigned short i, const unsigned short j, unsigned short *k) {
    *k = i*9+j;
}

/*
void idx_cell2mat(const unsigned short k, unsigned short *i, unsigned short *j) {
    *i = k / 9;
    *j = k % 9;
}
*/

void count_bits(const unsigned short input, unsigned short *count, unsigned short *idx) {
    /// Count number of bits set as 1 on an input integer as well as its
    /// last index position.
    *count = 0;
    *idx = 0;
    unsigned short temp = input;
    unsigned short temp_idx = 0;
    while (temp != 0) {
        if(temp & 1u) {
            *count += 1;
            *idx = temp_idx;
        }
        temp_idx += 1;
        temp = temp >> 1u;
        // printf("%d\n", temp);
    }
}

unsigned int sync_grid_and_guess(unsigned short* grid, unsigned short* guess) {
    /// Sync grid and guess arrays.
    ///
    /// When a grid cell is filled, its corresponding guess array is
    /// set to zero (no more guesses).
    /// When a cell's guess array is zero except for one number(only this
    /// number can fit this cell), said number is copied to its cell and
    /// array is set to zero.
    unsigned int op_count = 0;
    unsigned short bits = 0;
    unsigned short bit_idx = 0;
    for (unsigned short k_cell=0; k_cell < 81; ++k_cell) {

        // Non-zero value, cell is filled, nullify guesses.
        if (grid[k_cell]) {
            for (unsigned short ii = 0; ii < 9; ++ii) guess[k_cell] = 0u;
        }

        // Count guess bits. If 1, only one number fits in cell, use it.
        count_bits(guess[k_cell], &bits, &bit_idx);
        if (bits == 1) {
            grid[k_cell] = bit_idx + 1;
            guess[k_cell] = 0u;
            op_count += 1;
        }
    }
    return op_count;
}

void sweep(const unsigned short* grid, unsigned short* guess) {
    /// For each filled cell, delete cell guesses corresponding to
    /// the filled value on same (row|column|box).
    unsigned short v;
    unsigned short k;
    unsigned short ii;
    unsigned short jj;
    unsigned short kk;
    unsigned short tt;

    // Loop over grid rows.
    for (unsigned short i = 0; i < 9; ++i) {
        // Loop over grid cols.
        for (unsigned short j = 0; j < 9; ++j) {
            idx_mat2cell(i, j, &k);
            v = grid[k];

            // Cell is not empty, delete existing
            // adjacent possibilities.
            if (v) {

                // Sweep rows.
                for (ii = i; ii < i + 1; ++ii) {
                    for (jj = 0; jj < 9; ++jj) {
                        idx_mat2cell(ii, jj, &kk);
                        guess[kk] = guess[kk] & (MAX_GUESS - (1u << (v - 1u)));
                    }
                }

                // Sweep cols.
                for (ii = 0; ii < 9; ++ii) {
                    for (jj = j; jj < j+1; ++jj) {
                        idx_mat2cell(ii, jj, &kk);
                        guess[kk] = guess[kk] & (MAX_GUESS - (1u << (v - 1u)));
                    }
                }

                // Sweep box.
                for (tt = 0; tt < 9; ++tt) {
                    ii = 3*(i/3) + tt / 3;
                    jj = 3*(j/3) + tt % 3;
                    idx_mat2cell(ii, jj, &kk);
                    guess[kk] = guess[kk] & (MAX_GUESS - (1u << (v - 1u)));
                }
            }
        }
    }
}

unsigned int only_choice(unsigned short* grid, unsigned short* guess) {
    /// If X value guess only occurs on a cell inside a row, column or
    /// box, said value is set on cell.
    unsigned int op_count = 0;
    unsigned short value;
    unsigned short count;
    unsigned short k;
    unsigned short ii;
    unsigned short jj;
    unsigned short kk;
    unsigned short tt;

    // Loop over possible lone grid values.
    for (value = 1; value <= 9; ++value) {
        // Loop over nine (rows |  cols | boxes).
        for (tt = 0; tt < 9; ++tt) {

            // Check rows.
            count = 0;
            ii = tt;
            for (jj = 0; jj < 9; ++jj) {
                idx_mat2cell(ii, jj, &kk);
                // Value is not discarded, increment value counter.
                if ((guess[kk] & (1u << (value - 1u)))) {
                    count += 1;
                    k = kk;
                }
            }
            // Only one cell may fit this value, set it.
            if (count == 1) {
                grid[k] = value;
                guess[k] = 0;
                sweep(grid, guess);
                op_count += 1;
            }

            // Sweep cols.
            count = 0;
            jj = tt;
            for (ii = 0; ii < 9; ++ii) {
                idx_mat2cell(ii, jj, &kk);
                if ((guess[kk] & (1u << (value - 1u)))) {
                    count += 1;
                    k = kk;
                }
                // guess[kk] = guess[kk] & (MAX_GUESS - (1u << (v - 1u)));
            }
            if (count == 1) {
                grid[k] = value;
                guess[k] = 0;
                sweep(grid, guess);
                op_count += 1;
            }

            // Sweep box.
            count = 0;
            for (ii = 0; ii < 9; ++ii) {
                kk = 27 * (tt/3) + 3 * (tt%3)+ 9 * (ii/3) + (ii % 3);
                if ((guess[kk] & (1u << (value - 1u)))) {
                    count += 1;
                    k = kk;
                }
            }
            if (count == 1) {
                grid[k] = value;
                guess[k] = 0;
                sweep(grid, guess);
                op_count += 1;
            }
        }
    }
    return op_count;
}

void cross_guess(const unsigned short* grid, unsigned short* guess) {
    /// In case that a value fits several box cells but these cells
    /// are aligned, delete guesses on this cross section.
    unsigned short value;
    unsigned short count;
    unsigned short k;
    unsigned short ii;
    unsigned short jj;
    unsigned short kk;
    unsigned short tt;
    unsigned short r;
    unsigned short c;
    unsigned short b;
    unsigned short temp;
    unsigned short do_action;
    unsigned short do_action_r;
    unsigned short do_action_c;
    unsigned short box_findings[9];

    // Loop over possible lone grid values.
    for (value = 1; value <= 9; ++value) {
        // Loop over nine (rows |  cols | boxes).
        for (tt = 0; tt < 9; ++tt) {

            // Check rows.
            count = 0;
            b = 10;
            do_action = 0;
            ii = tt;
            for (jj = 0; jj < 9; ++jj) {
                idx_mat2cell(ii, jj, &kk);
                // Value is not discarded, increment value counter.
                if ((guess[kk] & (1u << (value - 1u)))) {
                    count += 1;
                    temp = 3 * (ii / 3) + jj / 3;
                    k = kk;
                    // Previously found shared box does not match new: no cross.
                    if((b < 9) && (b != temp)) {
                        do_action = 0;
                    }
                    else {
                        b = temp;
                        do_action = 1;
                    }
                }
            }
            // Cross at row tt: remove value from box guesses.
            if (do_action) {
                for (ii = 3*(b/3); ii < (unsigned short) (3u*(1u+b/3)); ++ii) {
                    for (jj = 3*(b%3); jj < (unsigned short) (3u*(1+(b%3))); ++jj) {
                        if (ii != tt) {
                            idx_mat2cell(ii, jj, &kk);
                            guess[kk] = guess[kk] & (MAX_GUESS - (1u << (value - 1u)));
                        }
                    }
                }
            }

            // Check cols.
            count = 0;
            b = 10;
            do_action = 0;
            jj = tt;
            for (ii = 0; ii < 9; ++ii) {
                idx_mat2cell(ii, jj, &kk);
                // Value is not discarded, increment value counter.
                if ((guess[kk] & (1u << (value - 1u)))) {
                    count += 1;
                    temp = 3 * (ii / 3) + jj / 3;
                    k = kk;
                    // Previously found shared box does not match new: no cross.
                    if((b < 9) && (b != temp)) {
                        do_action = 0;
                    }
                    else {
                        b = temp;
                        do_action = 1;
                    }
                }
            }
            // Cross at col tt: remove value from box guesses.
            if (do_action) {
                for (ii = 3*(b/3); ii < (unsigned short) (3u*(1+b/3)); ++ii) {
                    for (jj = 3*(b%3); jj < (unsigned short) (3u*(1+(b%3))); ++jj) {
                        if (jj != tt) {
                            idx_mat2cell(ii, jj, &kk);
                            guess[kk] = guess[kk] & (MAX_GUESS - (1u << (value - 1u)));
                        }
                    }
                }
            }

            // Sweep box.
            count = 0;
            r = 10;
            c = 10;
            do_action_r = 0;
            do_action_c = 0;
            b = tt;
            k = 0;
            for (ii = 0; ii < 9; ++ii) box_findings[ii] = 0u;

            for (ii = 3*(b/3); ii < (unsigned short) (3*(1+b/3)); ++ii) {
                for (jj = 3*(b%3); jj < (unsigned short) (3*(1+(b%3))); ++jj) {
                    // printf("I: %d, J: %d\n", ii, jj);
                    idx_mat2cell(ii, jj, &kk);
                    // Value is not discarded, increment value counter.
                    if ((guess[kk] & (1u << (value - 1u)))) {
                        box_findings[count] = kk;
                        count += 1;
                    }
                }
            }
            if (count > 1) {
                do_action_r = 1;
                // printf("KK: %u\n", box_findings[0]);
                r = box_findings[0] / 9;
                for (ii = 1; ii < count; ++ii) {
                    do_action_r = do_action_r && (r == box_findings[ii] / 9);
                }

                do_action_c = 1;
                c = box_findings[0] % 9;
                for (jj = 1; jj < count; ++jj) {
                    do_action_c = do_action_c && (c == box_findings[jj] % 9);
                }
            }

            // Cross at box b, row r: remove value from row.
            if (do_action_r) {
                // printf("B: %d, R: %d\n", b, r);
                // print_sudoku(grid);
                ii = r;
                for (jj = 0; jj < 9; ++jj) {
                    temp = 3 * (ii / 3) + jj / 3;
                    if (temp != b) {
                        idx_mat2cell(ii, jj, &kk);
                        guess[kk] = guess[kk] & (MAX_GUESS - (1u << (value - 1u)));
                    }
                }
            }
            if (do_action_c) {
                // printf("B: %d, C: %d\n", b, c);
                // print_sudoku(grid);
                jj = c;
                for (ii = 0; ii < 9; ++ii) {
                    temp = 3 * (ii / 3) + jj / 3;
                    if (temp != b) {
                        idx_mat2cell(ii, jj, &kk);
                        guess[kk] = guess[kk] & (MAX_GUESS - (1u << (value - 1u)));
                    }
                }
            }
        }
    }
}

void make_up(unsigned short* grid, unsigned short* guess) {
    /// Choose one valid guess from the cell with the least number of
    /// possible values, set it, solve it and, in case of faulty Sudoku,
    /// restore it and switch the guess value.
    ///
    /// Parameters:
    /// ===========
    ///
    /// unsigned short grid[81] : In-out 9x9 Sudoku grid.
    /// unsigned short guess[81] : In-out Sudoku grid's guesses.
    // Temp Sudoku.
    unsigned short grid2[81];
    unsigned short k;
    unsigned short v = 81;
    int st = 0;

    // Get the cell index (k) with the least number of guess values (v).
    for (unsigned short i = 0; i < 81; ++i) {
        unsigned short kk, vv;
        count_bits(guess[i], &vv, &kk);
        // Note! if vv == 0, cell is filled, it does not count.
        if (vv < v && vv > 0) {
            v = vv;
            k = i;
        }
    }

    // Loop over 1-9 cell values.
    for (unsigned short i = 0; i < 9; ++i) {
        // Loop value is valid guess.
        if (guess[k] & (1u << i)) {
            // Back-up.
            memcpy(grid2, grid, 81 * sizeof *grid);
            // Set value.
            grid2[k] = i + 1u;
            // Solve.
            st = solve(grid2);

            // If Sudoku is valid and complete, exit loop.
            if (st) break;
        }
    }
    // Restore complete Sudoku on base Sudoku.
    memcpy(grid, grid2, 81 * sizeof *grid);
    // Sync grid and guesses.
    sync_grid_and_guess(grid, guess);
}

int check_complete(const unsigned short* grid) {
    /// Check that Sudoku is valid and complete.
    ///
    /// Parameters:
    /// ===========
    ///
    /// const unsigned short grid[81] : Input 9x9 Sudoku grid.
    ///
    /// Returns:
    /// ========
    ///
    /// int : Sudoku status:
    ///           1 => Valid and complete Sudoku.
    ///           0 => Faulty or incomplete Sudoku.
    unsigned short value;
    unsigned short count;
    unsigned short ii;
    unsigned short jj;
    unsigned short kk;
    unsigned short tt;

    // Loop over possible lone grid values.
    for (value = 1; value <= 9; ++value) {
        // Loop over nine (rows |  cols | boxes).
        for (tt = 0; tt < 9; ++tt) {

            // Check rows.
            count = 0;
            ii = tt;
            for (jj = 0; jj < 9; ++jj) {
                idx_mat2cell(ii, jj, &kk);
                // Loop value is cell value, increment counter.
                if (grid[kk] == value) {
                    count += 1;
                }
            }
            // If not unique (or existing) value, return false.
            if (count != 1) return 0;

            // Sweep cols.
            count = 0;
            jj = tt;
            for (ii = 0; ii < 9; ++ii) {
                idx_mat2cell(ii, jj, &kk);
                if (grid[kk] == value) {
                    count += 1;
                }
            }
            if (count != 1) return 0;

            // Sweep box.
            count = 0;
            for (ii = 0; ii < 9; ++ii) {
                kk = 27 * (tt/3) + 3 * (tt%3)+ 9 * (ii/3) + (ii % 3);
                if (grid[kk] == value) {
                    count += 1;
                }
            }
            if (count != 1) return 0;
        }
    }
    return 1;
}

int check_valid(const unsigned short* grid, const unsigned short* guess) {
    /// Check that Sudoku (and guesses) are valid.
    ///
    /// Parameters:
    /// ===========
    ///
    /// const unsigned short grid[81] : Input 9x9 Sudoku grid.
    /// const unsigned short guess[81] : Input 9x9 Sudoku guesses.
    ///
    /// Returns:
    /// ========
    ///
    /// int : Sudoku status:
    ///           1 => Valid (or valid and complete) Sudoku.
    ///           0 => Faulty Sudoku.
    unsigned short value;
    unsigned short count;
    unsigned short ii;
    unsigned short jj;
    unsigned short kk;
    unsigned short tt;

    // Loop over possible lone grid values.
    for (value = 1u; value <= 9u; ++value) {
        // Loop over nine (rows |  cols | boxes).
        for (tt = 0u; tt < 9u; ++tt) {

            // Check rows.
            count = 0u;
            ii = tt;
            for (jj = 0u; jj < 9u; ++jj) {
                idx_mat2cell(ii, jj, &kk);
                // Loop value is cell value, increment counter.
                if (grid[kk] == value) {
                    count += 1u;
                }
            }
            // If not unique (or existing) value, return false.
            if (count > 1u) {
                //printf("I: Fault at Row %d, Value %d.\n", tt, value);
                return 0;
            }

            // Sweep cols.
            count = 0u;
            jj = tt;
            for (ii = 0u; ii < 9u; ++ii) {
                idx_mat2cell(ii, jj, &kk);
                if (grid[kk] == value) {
                    count += 1u;
                }
            }
            if (count > 1u) {
                //printf("I: Fault at Column %d, Value %d.\n", tt, value);
                return 0;
            }

            // Sweep box.
            count = 0u;
            for (ii = 0u; ii < 9u; ++ii) {
                kk = 27u * (tt/3) + 3u * (tt%3)+ 9u * (ii/3) + (ii % 3);
                if (grid[kk] == value) {
                    count += 1u;
                }
            }
            if (count > 1) {
                //printf("I: Fault at Box %d, Value %d.\n", tt, value);
                return 0;
            }
        }
    }
    //printf("I: Is valid.\n");

    // Check that all zero guess cells do have a cell value.
    for (kk = 0; kk < 81; ++kk) {
        if (guess[kk] == 0u && grid[kk] == 0) return 0;
    }
    return 1;
}

int solve(unsigned short* grid) {
    /// Solve a sudoku.
    ///
    /// Parameters:
    /// ===========
    ///
    /// unsigned short grid[81] : In-out 9x9 Sudoku grid as 81x1 C-array, with 0 as empty cells.
    ///
    /// Returns:
    /// ========
    ///
    /// int : Solved sudoku status:
    ///           1 => Completed valid Sudoku.
    ///           0 => Faulty or incomplete Sudoku.
    // Possible numbers that a cell may contain.
    unsigned short guess[81];
    // Loop counter.
    unsigned long count = 0;
    // Output Sudoku status.
    int status = 0;
    // Operation counter.
    unsigned int op_count = 1;

    // Initialize guess grid.
    for(short k=0;k<81;++k) *(guess+k)=MAX_GUESS;
    sync_grid_and_guess(grid, guess);

    // Perform operations. If no operations are performed, exit.
    while (op_count) {
        // Reset operation counter.
        op_count = 0;

        // Delete guesses.
        sweep(grid, guess);
        // If only one possible value on cell, set it.
        op_count += only_choice(grid, guess);
        // Delete guesses if multiple value guesses on box are aligned.
        cross_guess(grid, guess);
        // Coordinate sudoku and guesses.
        op_count += sync_grid_and_guess(grid, guess);

        // Increase counter and eval exit condition.
        count += 1;
        status = check_complete(grid);
        if (status) break;
    }

    // Sudoku is valid but not complete, make bold guesses, re-solve and re-check.
    if (check_valid(grid, guess) == 1 && ! status) {
        make_up(grid, guess);
        status = check_complete(grid);
    }
    return status;
}