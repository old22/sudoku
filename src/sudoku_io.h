void load_sudoku(const char* sudoku_str, unsigned short* grid);
void load_gameid(const char* id, unsigned short* grid);
void print_sudoku(const unsigned short* grid);
void print_gameid(const unsigned short* grid);
