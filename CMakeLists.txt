cmake_minimum_required(VERSION 3.16)
project(sudoku)

set(CMAKE_C_STANDARD 99)
set(CMAKE_CXX_STANDARD 20)
# enable_testing()

set(CMAKE_FLAGS_DEBUG  "-g -O0 -Wall -Wextra")
set(CMAKE_FLAGS_RELEASE  "-O3 -Wall -Wextra")

add_library(sudoku src/sudoku.c src/sudoku.h src/sudoku_io.h src/sudoku_io.c)

add_executable(main src/main.c)
target_link_libraries(main sudoku)
# include_directories(main src)
# link_libraries(main sudoku)

add_executable(test src/test.c)
target_link_libraries(test sudoku)
# include_directories(test src)
# link_libraries(test sudoku)
# add_test(NAME sudoku_test COMMAND sudoku ".................85.7.1..2...........3...6.....9..523..6...3.1...18...54.4.69...7")