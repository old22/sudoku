
# Sudoku

Quick Sudoku solver.

## Usage.

It can be used either by the main executable
or by using the library:

1. Main executable, pass as argument a string of 81
   characters where chars 1-9 are input Sudoku's
   cell values and 0 (or any other char)is empty
   cell. Sudoku cells are passed by rows:
   ```bash
   $ main 001700509573024106800501002700295018009400305652800007465080071000159004908007053
   ```
2. Library, link the sudoku library and use the
   ``src/sudoku.h`` function, where function argument
   is the Sudoku as 81-sized array to be modified and
   output is the solved Sudoku state. This array
   can be initialized using functions defined in
   ``src/sudoku_io.h`` (i.e. Simon Tatham's Game Id):
   ```c
   #include "sudoku.h"
   #include "sudoku_io.h"

   const char* S_RAW = "001700509573024106800501002700295018009400305652800007465080071000159004908007053";
   const char* S_GAMEID = "3x3:b6_5_3a4_8a5b2c7a9_8a4e4d5_2c6e4c8_9d6e4a6_8a4c1b5a1_9a5_6_7b";
   
   int main() {
       int st;  // Returned Sudoku's state.
       unsigned short grid[81];  // Uninitialized Sudoku.
   
       load_sudoku(S_RAW, grid);  // Init from raw data...
       load_gameid(S_GAMEID, grid);  // ...or from Game ID.
   
       // Solve, print and quit.
       st = solve(grid);
       print_sudoku(grid);
       return st;
   }
   ```
   
## References.

[Simon Tatham's Portable Puzzle Collection](https://www.chiark.greenend.org.uk/~sgtatham/puzzles/)